from flask import (
    Flask,
    session,
    render_template,
    request,
    abort,
    redirect,
    url_for,
    jsonify
)

from .config import Config
from .database import db
from .models.game import Game

import uuid, random

app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)

with app.app_context():
    db.create_all()

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/init')
def init():
    cards = {
            1: 'free',
            2: 'free',
            3: 'free'
    }
    if not 'id' in session:
        session['id'] = uuid.uuid1()
        session['car'] = random.randint(1, 3)
    elif 'first' in session:
        cards[session['first']] = 'selected'
        cards[session['dismissed']] = 'dismissed'

    return jsonify(cards)

@app.route('/play', methods=['POST'])
def play():
    card = int(request.json['card'])
    if not 'id' in session:
        abort(401)
    elif card not in [1, 2, 3]:
        abort(400, description='Card: {}'.format(card))
    elif not 'first' in session:
        session['first'] = card
        session['dismissed'] = random.choice([ x for x in [1, 2, 3] if x not in [card, session['car']] ])
        res = {
            'step': 'first',
            'dismiss': str(session['dismissed'])
        }
        return jsonify(res)
    else:
        session['final'] = card
        res = {
            'step': 'final',
            'car': session['car']
        }

        g = Game(
                first=session['first'],
                final=session['final'],
                dismissed=session['dismissed'],
                car=session['car'],
                success=session['final']==session['car'])

        db.session.add(g)
        db.session.commit()

        session.clear()
        return jsonify(res)


@app.route('/reset')
def reset():
    session.clear()
    return redirect(url_for("home"))

@app.route('/results')
def results():
    games = Game.query.all()

    res = {
        'global': {
            'success': len([g for g in games if g.success]),
            'failure': len([g for g in games if not g.success])
        },
        'keep': {
            'success': len([g for g in games if g.success and g.first == g.final]),
            'failure': len([g for g in games if not g.success and g.first == g.final])
        },
        'change': {
            'success': len([g for g in games if g.success and g.first != g.final]),
            'failure': len([g for g in games if not g.success and g.first != g.final])
        },
        'choice': {
            'keep': len([g for g in games if g.first == g.final]),
            'change': len([g for g in games if g.first != g.final])
        },
        'cars': {
            1: len([g for g in games if g.car == 1]),
            2: len([g for g in games if g.car == 2]),
            3: len([g for g in games if g.car == 3])
        },
        'first': {
            1: len([g for g in games if g.first == 1]),
            2: len([g for g in games if g.first == 2]),
            3: len([g for g in games if g.first == 3])
        }
    }

    return jsonify(res)
