from monty_hall.database import db

from datetime import datetime

class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first = db.Column(db.Integer, nullable=False)
    final = db.Column(db.Integer, nullable=False)
    dismissed = db.Column(db.Integer, nullable=False)
    car = db.Column(db.Integer, nullable=False)
    success = db.Column(db.Boolean, nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
