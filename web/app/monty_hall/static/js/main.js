let lock = false;
const req = new XMLHttpRequest();
req.open("GET", "/init");
req.send();
req.onload = function(){
	if (this.status >= 200 && this.status < 400){
		const cards = JSON.parse(this.response);

		for (c in cards){
			if (cards[c] == "dismissed"){
				const dismissed =	document.getElementById(`card-${c}`);
				dismissed.classList.add("dismissed");
			}
			else if (cards[c] == "selected"){
				document.getElementById(`card-${c}`).classList.add("selected");
			}
		}
	}
}

const available_cards = document.querySelectorAll(".card:not(.dismissed)");

for (c of available_cards){
	c.addEventListener("click", selectCard);
}

function selectCard(){
	if (lock){
		return null;
	}
	lock = true;
	const card = parseInt(this.getAttribute("id").substring(5, 6));

	const req = new XMLHttpRequest();
	req.open("POST", "/play");
	req.setRequestHeader("Content-Type", "application/json");
	req.send(JSON.stringify({ "card": card }));

	req.onload = function(){
		if (this.status >= 200 && this.status < 400){
			res = JSON.parse(this.response);

			if (res["step"] == "first"){
				const dismissed = document.getElementById(`card-${res["dismiss"]}`);
				dismissed.classList.add("dismissed");
				dismissed.removeEventListener("click", selectCard);

				const selected = document.getElementById(`card-${card}`);
				selected.classList.add("selected");

				const hint = document.getElementById("hint");
				hint.querySelector("strong").textContent = res["dismiss"];
				hint.classList.remove("d-none");
			}
			else if (res["step"] == "final"){
				cards = document.querySelectorAll(".card");

				for (c of cards){
					c.classList.remove("selected");
				}

				document.getElementById(`card-${res["car"]}`).classList.add("correct");

				if (res["car"] != card){
					document.getElementById(`card-${card}`).classList.add("error");
				}
				else{
					document.querySelector(".card:not(.dismissed):not(.correct)").classList.add("dismissed");
				}

				showResults();
			}

			lock = false;
		}
	}
}

function generatePieChart(id, data, colors = ['#dc3545', '#28a745']){
	var w=100, h=100;
	var margin = 5;
	var radius = Math.min(w, h) / 2 - margin;

	var svg = d3.select(id)
		.append("svg")
		.attr("width", w)
		.attr("height", h)
		.append('g')
		.attr("transform", "translate("+ w/2 +", "+ h/2 +")");

	var color = d3.scaleOrdinal()
		.domain(data)
		.range(colors);

	var pie = d3.pie()
		.sort(null)
		.value(d => d.value);

	var data_ready = pie(d3.entries(data));

	svg
		.selectAll('whatever')
		.data(data_ready)
		.enter()
		.append('path')
		.attr('d', d3.arc()
			.innerRadius(0)
			.outerRadius(radius)
		)
		.attr('fill', d => color(d.data.key));
}

function showResults(){
	console.log("results");
	const reset = document.getElementById("reset");

	reset.classList.remove("d-none");

	const req = new XMLHttpRequest()

	req.open("GET", "/results");
	req.send();

	req.onload = function(){
		if (this.status >= 200 && this.status < 400){
			const data = JSON.parse(this.response)

			// Pie Charts
			generatePieChart('#chart-global', data['global']);
			generatePieChart('#chart-keep', data['keep']);
			generatePieChart('#chart-change', data['change']);
			generatePieChart('#chart-choice', data['choice'], ['#e0a800', '#138496']);

			// Texts
			document.querySelector('strong#nb-keep-success').textContent = data['keep']['success'];
			document.querySelector('strong#nb-keep-failure').textContent = data['keep']['failure'];
			document.querySelector('strong#per-keep-success').textContent = Math.round(100 * data['keep']['success'] / (data['keep']['success'] + data['keep']['failure']));
			document.querySelector('strong#per-keep-failure').textContent = Math.round(100 * data['keep']['failure'] / (data['keep']['success'] + data['keep']['failure']));

			document.querySelector('strong#nb-change-success').textContent = data['change']['success'];
			document.querySelector('strong#nb-change-failure').textContent = data['change']['failure'];
			document.querySelector('strong#per-change-success').textContent = Math.round(100 * data['change']['success'] / (data['change']['success'] + data['change']['failure']));
			document.querySelector('strong#per-change-failure').textContent = Math.round(100 * data['change']['failure'] / (data['change']['success'] + data['change']['failure']));

			document.querySelector('strong#nb-global-success').textContent = data['global']['success'];
			document.querySelector('strong#nb-global-failure').textContent = data['global']['failure'];
			document.querySelector('strong#per-global-success').textContent = Math.round(100 * data['global']['success'] / (data['global']['success'] + data['global']['failure']));
			document.querySelector('strong#per-global-failure').textContent = Math.round(100 * data['global']['failure'] / (data['global']['success'] + data['global']['failure']));

			document.querySelector('strong#nb-choice-keep').textContent = data['choice']['keep'];
			document.querySelector('strong#nb-choice-change').textContent = data['choice']['change'];
			document.querySelector('strong#per-choice-keep').textContent = Math.round(100 * data['choice']['keep'] / (data['choice']['keep'] + data['choice']['change']));
			document.querySelector('strong#per-choice-change').textContent = Math.round(100 * data['choice']['change'] / (data['choice']['keep'] + data['choice']['change']));

			const results = document.getElementById("results");
			results.classList.remove("d-none");
		}
	}
}
